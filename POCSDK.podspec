
Pod::Spec.new do |s|
    s.name          = "POCSDK"
    s.version       = "0.0.1"
    s.summary       = "iOS SDK for POCSDK"
    s.description   = "iOS SDK for POCSDK, including example app"
    s.homepage      = "https://www.vuedata.com/"
    s.license       = 'MIT'
    s.author        = "saravanakumar"
    s.ios.deployment_target = '11.0'
    s.swift_version = '5.0'
    s.source        = {
      :git => "https://skvuedata@bitbucket.org/skvuedata/pocsdk.git",
      :tag => "#{s.version}"
    }
    s.source_files        = "CPaaSSDK/**/*.{h,m,swift}"
    s.public_header_files = "CPaaSSDK/**/*.h"
    s.dependency "Moya", "14.0"
  end