//
//  CPaaSPlayer.swift
//  CPaaSSDK

import UIKit
import AVFoundation
import AVKit

public protocol CPaaSPlayerDelegate: class {
    func btnActionClose()
}

public class CPaaSPlayer: UIView {
    static let staticVideoUrl = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4"
    let nibName = "CPASSPlayer"
    var contentView: UIView!
    var player: AVPlayer!
    var urlVal: String = ""
    public weak var delegate: CPaaSPlayerDelegate!
    @IBOutlet weak var btnClose: UIButton!
    
    // MARK: Set Up View
    public override init(frame: CGRect) {
        // For use in code
        super.init(frame: frame)
        setUpView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        // For use in Interface Builder
        super.init(coder: aDecoder)
        setUpView()
    }
    
    private func setUpView() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.nibName, bundle: bundle)
        self.contentView = nib.instantiate(withOwner: self, options: nil).first as? UIView
        addSubview(contentView)
        contentView.center = self.center
        contentView.autoresizingMask = []
        contentView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    public func playerView(model:CPaaSModel,videoUrl: String){
        urlVal = videoUrl
        let tententKey = model.setUserKey
        switch tententKey {
        case "Tentant1":
            urlVal = CPaaSPlayer.staticVideoUrl
        case "Tentant2":
            self.customPlayer(videoUrlVal: videoUrl)
        default:
            print("\(model.setUserKey)")
            if videoUrl.count == 0{
                urlVal = CPaaSPlayer.staticVideoUrl
            }
        }
        print("[Name:\(tententKey),PlayerUrl:\(videoUrl)]")
    }
    
    func customPlayer(videoUrlVal: String){
        let videoURL = URL(string: videoUrlVal)
        self.player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: self.player)
        playerLayer.frame = self.contentView.bounds
        self.contentView.layer.addSublayer(playerLayer)
        self.player.play()
    }
    
    @IBAction func btnActionClose( _ sender: Any){
        self.player.pause()
        self.delegate.btnActionClose()
    }
}
