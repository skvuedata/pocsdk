//
//  CPaaSKey.swift
//  CPaaSSDK


import Foundation

public struct CPaaSModel{
    public private(set) var isDownload: Bool
    public private(set) var isVideoUrlEditable: Bool
    public private(set) var isVideoPlayerColorChangeble: Bool
    public private(set) var isListCount: Int
    public private(set) var setUserKey: String
    public private(set) var forceUpdateKey: Bool
}

public class CPaaSKey{
    public init() {}
    public func UserKey(key: String) -> CPaaSModel{
        switch key {
        case "Tentant1":
            print("\(key)")
            return CPaaSModel(isDownload: false, isVideoUrlEditable: false, isVideoPlayerColorChangeble: false, isListCount: 5,setUserKey: key, forceUpdateKey: false)
        case "Tentant2":
            print("\(key)")
            return CPaaSModel(isDownload: true, isVideoUrlEditable: true, isVideoPlayerColorChangeble: true, isListCount: 10,setUserKey: key, forceUpdateKey: false)
        case "Tentant3":
            print("\(key)")
            return CPaaSModel(isDownload: true, isVideoUrlEditable: true, isVideoPlayerColorChangeble: true, isListCount: 10,setUserKey: key, forceUpdateKey: true)
        default:
            print("User Key")
            return CPaaSModel(isDownload: false, isVideoUrlEditable: false, isVideoPlayerColorChangeble: false, isListCount: 5,setUserKey: key, forceUpdateKey: false)
        }
    }
}
