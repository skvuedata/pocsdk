//
//  Path.swift
//  CPaaSSDK


import Moya


extension APITarget: TargetType{
    // BASE_URL
    public var baseURL: URL {
        return URL(string: API.BASE.PROD.URL)!
    }
    // PATH
    public var path: String {
        switch self {
        case .categories:
            return API.PATH.categories
        case .design:
            return API.PATH.design
            
        }
    }
    
    // MEthod
    public var method: Moya.Method {
        return .get
    }

    
    public var headers: [String : String]? {
        return nil
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    // TASK
    public var task: Task {
        switch self {
        case .categories,.design:
            return .requestPlain
        }
    }

}
