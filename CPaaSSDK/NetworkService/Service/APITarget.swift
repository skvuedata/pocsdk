//
//  Apitarget.swift
//  CPaaSSDK

import Foundation
import Moya

public enum APITarget {
    case categories
    case design
}
