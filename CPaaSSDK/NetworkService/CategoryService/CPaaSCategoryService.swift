//
//  CPaaSCategoryService.swift
//  CPaaSSDK
//


import Foundation
import Moya

public class CPaaSCategoryService: NSObject {
    
    let provider = MoyaProvider<APITarget>()
    var categoryModelVal:CPaaSCategoryModel?
    
    public func getCategoryList(staticClassCount: Int,onSuccess: @escaping (CPaaSCategoryModel)-> (),onFailure: @escaping (String)-> ()){
        provider.request(.categories) { (response) in
            switch response{
            case .success(_):
                do{
                    let fetchResult = try response.get().data
                    let decoder = JSONDecoder()
                    if let json = try? decoder.decode(CPaaSCategoryModel.self, from: fetchResult){
                        onSuccess(json)
                    }else{
                        onFailure(Content.ErrorContent.error)
                    }
                } catch{
                    onFailure(Content.ErrorContent.error)
                }
            case .failure(_):
                onFailure(Content.ErrorContent.error)
            }
        }
    }
}
