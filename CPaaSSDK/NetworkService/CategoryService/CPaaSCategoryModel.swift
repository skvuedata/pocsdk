//
//  CategoryModel.swift
//  CPASSSDK

import Foundation

// MARK: - CategoryModel
public struct CPaaSCategoryModel: Codable {
   public  let movies: [Movie]?
}

// MARK: - Movie
public struct Movie: Codable {
   public let id, image, title ,subTitle: String?
   private let video: String?
}
