//
//  ConstantAPI.swift
//  CPASSSDK

import Foundation

struct API {
    struct BASE {
        struct PROD {
            static let URL = "https://api.mocki.io/v1"
        }
        struct DEV {
            static let URL = "https://api.mocki.io/v1"
        }
    }
    struct PATH {
        static let categories = "/7cbbafc7"
        static let design =   "/8332400d" //"/fd44e474"
    }
}

struct Content {
    struct ErrorContent {
        static let error = "Error"
        static let failToFetch = "Category Failed To Fetch"
    }
}
